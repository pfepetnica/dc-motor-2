// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float napon = 0;
}

// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
class Measurement
{
  float ugao = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// metode Update, Measure, Control, Visualize
class MotorSimulacija extends Simulation
{
  float ugaonaBrzina = 0;
  //float napon = 2;
  float dt = 0;
  float dUgaonaBrzina = 0;
  float T = 1;
  float G = 5;
  float vreme = 0;

  float medju = 0;
  float Ref = 0;
  float e = 0;
  float de = 0;
  float staroe = 0;
  
  float x = 0;
  float y = 0;
  float ugao = 0;
 
  PrintWriter printWriter = createWriter("positions.txt");
  

  MotorSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);
    
    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);
    
    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    dUgaonaBrzina = (dt/T)*(input.napon*G-ugaonaBrzina);
    ugaonaBrzina = ugaonaBrzina + dUgaonaBrzina;
    vreme = vreme + dt;
    ugao = ugao + ugaonaBrzina * dt;
    x = sin(ugao);
    y = cos(ugao);
    println(ugao);
  }

  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.ugao = ugao +randomGaussian()/10;
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    Actuation o = new Actuation();
    //e = 0.174235 * (Ref - m.ugao);
    e = 0.268758 * (Ref - m.ugao);
    //e = 0.5268758 * (Ref - m.ugao);
    de = (e-staroe)/dt;
    
    staroe=e;
    float k = de + e;
    
    o.napon = k;
    
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    
    noStroke();
    fill(205, 10);
    rect(width/2-100, height/2-100, 200, 200);
    fill(255);
    stroke(100,100,100);
    ellipse(width/2,height/2, 100,100);
    stroke(255, 0, 0);
    line(width/2,height/2, 100*x+width/2, 100*y+height/2);
    
    point(vreme*30, 360 - 10*ugao -1);
    //Ref = 6;
    Ref = mouseX/100.0;
    println(Ref);
    //stroke(0,0,0);
    //line(0, 0, 0, height);
    //line(0, height-1, width, height-1); 
    //199.99771
    //0.00228999999
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);
  
  sim = new MotorSimulacija();
}